FROM mariadb:10.4-bionic

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
 && apt-get install -y tango-db

ENV MYSQL_DATABASE=tango

RUN echo 'use tango;' > /docker-entrypoint-initdb.d/tango.sql \
 && cat /usr/share/dbconfig-common/data/tango-db/install/mysql > /docker-entrypoint-initdb.d/tango.sql

# RUN { \
#     echo '#!/bin/sh'; \
#     echo 'create database if not exists tango;' | mysql -p
#     echo '/usr/lib/tango/tango_admin --add-server TangoTest/$1 TangoTest sys/test/1'; \
#     echo 'exec /usr/lib/tango/TangoTest "$@"'; \
#     } | cat > /docker-entrypoint-initdb.d/tango.sh \
#     && chmod +x /entrypoint.sh
# ARG MYSQL_ROOT_PASSWORD=secret
# ENV MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD

# ARG MYSQL_DATABASE=tango

# RUN ( /docker-entrypoint.sh mysqld & ) \
#  && sleep 20 \
#  && mysql -p$MYSQL_ROOT_PASSWORD -D $MYSQL_DATABASE < /usr/share/dbconfig-common/data/tango-db/install/mysql \
#  && sleep 1
