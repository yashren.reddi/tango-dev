class: center, middle

# Tango and Docker containers
## Michal Liszcz (S2Innovation), Matteo Di Carlo (INAF)
## ICALEPCS 2019, New York

---

# Containers

1. Easy way for packaging applications and dependencies
1. Repeatable environments for development, build, testing and CI stages
1. Possibility to scale applictions: from dev's laptop to a prod. server
1. A combination of kernel primitives: namespaces, cgroups and chroot
1. **Docker** is an implementation of Open Container Initiative (OCI) spec.
1. Containers vs Virtual Machines (source: [docker.com](https://www.docker.com/resources/what-container)):

.cols[
.fifty[
![](https://www.docker.com/sites/default/files/d8/2018-11/docker-containerized-appliction-blue-border_2.png)
]
.fifty[
![](https://www.docker.com/sites/default/files/d8/2018-11/container-vm-whatcontainer_2.png)
]
]


---

# Tango on Docker—hands-on tutorial

1. Four Docker containers connected into a dedicated network
1. Materials available online: <https://tiny.cc/TangoDocker>


.center[

![](http://www.plantuml.com/plantuml/svg/RP31JeCX48Jl_rDCEEcXR-2XQRYQcBGliC9gYFmimhQcCRwxG4LZsWimau-Px6whKj7Z6XSNvUXXbDAh8B5-I7ap-5oIU6wNapLBugHuf1B8xa0Lwxc-n-NhcZ6Mb7PK6NPlEkcVVRq9FfqFlN50BeRctSG6TE2gkBFx-q5NBYSkZMwuCbGov6MwWETJS3mr9u-E_ODd8NuUe9nZSAH1qj-FtZ06nSDcSnca7rtDzAbduCLkJlorjYslhVaR)

]

---

# Hands-on (1/5)—Network and MariaDB

```shell
$ docker network create tango-nw
```

```shell
$ docker run -it --rm --name mariadb --network tango-nw \
             -e MYSQL_ROOT_PASSWORD=secret mariadb:10.4
```

---

# Hands-on (2/5)—DataBase DS

```shell
$ docker run -it --rm --name databaseds --network tango-nw ubuntu:bionic
```

```shell
$ export DEBIAN_FRONTEND=noninteractve
$ apt update
$ apt install -y tango-db
```

```shell
$ mysql -h mariadb -u root -psecret
```

```sql
mysql> create database tango;
mysql> exit
```

```shell
$ mysql -h mariadb -u root -psecret -D tango \
        < /usr/share/dbconfig-common/data/tango-db/install/mysql
```

```shell
$ TANGO_HOST=localhost:10000 MYSQL_HOST=mariadb:3306 MYSQL_USER=root \
  MYSQL_PASSWORD=secret MYSQL_DATABASE=tango \
  /usr/lib/tango/DataBaseds 2 -ORBendPoint giop:tcp::10000
```

---

# Hands-on (3/5)—TangoTest (device server)

```shell
$ docker run -it --rm --name tango-test --network tango-nw ubuntu:bionic
```

```shell
$ export DEBIAN_FRONTEND=noninteractve
$ apt update
$ apt install -y tango-test
```

```shell
$ export TANGO_HOST=databaseds:10000

$ /usr/lib/tango/tango_admin --add-server \
  TangoTest/test01 TangoTest icalepcs/test/1

$ /usr/lib/tango/TangoTest test01
```

---

# Hands-on (4/5)—PyTango (client app)

```shell
$ docker run -it --rm --name tango-client --network tango-nw ubuntu:bionic
```

```shell
$ export DEBIAN_FRONTEND=noninteractve
$ apt update
$ apt install -y python-pytango
```

```shell
$ export TANGO_HOST=databaseds:10000
$ python
```

```python
>>> import tango
>>> proxy = tango.DeviceProxy("icalepcs/test/1")
>>> proxy.Status()
'The device is in RUNNING state.'
>>> proxy.double_scalar
255.84406383401958
```

---

# Hands-on (5/5)—Jive (client app)

```shell
$ xhost +
$ docker run -it --rm --name jive --network tango-nw \
             -v /tmp/.X11-unix:/tmp/.X11-unix:ro \
             -e DISPLAY=$DISPLAY ubuntu:bionic
```

```shell
$ apt update
$ apt install -y wget openjdk-8-jre
$ wget https://dl.bintray.com/tango-controls/maven/org/tango/Jive/7.22/Jive-7.22-jar-with-dependencies.jar
```

```shell
$ export TANGO_HOST=databaseds:10000
$ java -jar Jive-7.22-jar-with-dependencies.jar
```

---

# Hands-on (5/5)—Jive (access from host)

```shell
CONTAINER_IP=$(docker inspect -f '{{(index .NetworkSettings.Networks "tango-nw").IPAddress}}' databaseds)
```

```shell
wget https://dl.bintray.com/tango-controls/maven/org/tango/Jive/7.22/Jive-7.22-jar-with-dependencies.jar
TANGO_HOST=$CONTAINER_IP:10000 java -jar Jive-7.22-jar-with-dependencies.jar
```

---

# Dockerfile

```dockerfile
# pytango.Dockerfile

FROM ubuntu:bionic

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
 && apt-get install -y python-pytango

ENTRYPOINT ["/usr/bin/python"]
```

```shell
$ docker build -t tango-pytango -f pytango.Dockerfile .
$ docker run -it --rm tango-pytango
```

```python
>>> import tango
```

---

# Docker Compose (1/2)

```shell
$ /usr/bin/tree
.
├── databaseds.Dockerfile
├── docker-compose.yaml
├── mariadb.Dockerfile
├── pytango.Dockerfile
└── tangotest.Dockerfile

0 directories, 5 files
```

```shell
$ docker-compose -p icalepcs up
...
tangotest_1   | Ready to accept request
```

```shell
$ docker attach icalepcs_pytango_1
```

```python
>>> import tango
>>> proxy = tango.DeviceProxy("sys/test/1")
>>> proxy.Status()
'The device is in RUNNING state.'
```

---

# Docker Compose (2/2)

.cols[
.fifty[

```yaml
version: '3'
services:

  mariadb:
    build:
      context: .
      dockerfile: mariadb.Dockerfile
    environment:
        MYSQL_ROOT_PASSWORD: secret

  databaseds:
    build:
      context: .
      dockerfile: databaseds.Dockerfile
    environment:
      TANGO_HOST: 'localhost:10000'
      MYSQL_HOST: 'mariadb:3306'
      MYSQL_USER: root
      MYSQL_PASSWORD: secret
```

]

.fifty[

```yaml
  tangotest:
    build:
      context: .
      dockerfile: tangotest.Dockerfile
    environment:
      TANGO_HOST: databaseds:10000

  pytango:
    stdin_open: true
    tty: true
    build:
      context: .
      dockerfile: pytango.Dockerfile
    environment:
      TANGO_HOST: databaseds:10000
```

]
]
