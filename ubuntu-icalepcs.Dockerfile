FROM ubuntu:bionic

# Ubuntu image for ICALEPCS workshop.
# All required packages are pre-downloaded to /var/cache/apt/archives.
# Bash history is pre-populated with all commands.

RUN apt-get update \
 && apt-get install -y wget less \
 && wget https://github.com/junegunn/fzf/archive/0.18.0.tar.gz \
 && tar xf *.tar.gz \
 && ./fzf-*/install --key-bindings --no-completion --update-rc \
 && rm *.tar.gz

RUN { \
    echo "export DEBIAN_FRONTEND=noninteractve"; \
    echo "apt install -y tango-db"; \
    echo "TANGO_HOST=localhost:10000 MYSQL_HOST=mariadb:3306 MYSQL_USER=root MYSQL_PASSWORD=secret MYSQL_DATABASE=tango /usr/lib/tango/DataBaseds 2 -ORBendPoint giop:tcp::10000"; \
    echo "mysql -h mariadb -u root -psecret"; \
    echo "mysql -h mariadb -u root -psecret -D tango < /usr/share/dbconfig-common/data/tango-db/install/mysql"; \
    echo "apt install -y tango-test"; \
    echo "export TANGO_HOST=databaseds:10000"; \
    echo "/usr/lib/tango/tango_admin --add-server TangoTest/test01 TangoTest icalepcs/test/1"; \
    echo "/usr/lib/tango/TangoTest test01"; \
    echo "apt install -y python-pytango"; \
    echo "python"; \
    echo "apt install -y openjdk-8-jre"; \
    echo "wget https://dl.bintray.com/tango-controls/maven/org/tango/Jive/7.22/Jive-7.22-jar-with-dependencies.jar"; \
    echo "java -jar Jive-7.22-jar-with-dependencies.jar"; \
    } > /root/.bash_history

RUN rm /etc/apt/apt.conf.d/docker-clean

RUN apt-get update \
 && apt-get install -y --download-only \
    tango-db \
    tango-test \
    python-pytango \
    openjdk-8-jre

RUN wget "https://dl.bintray.com/tango-controls/maven/org/tango/Jive/7.22/Jive-7.22-jar-with-dependencies.jar"
