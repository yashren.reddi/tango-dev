from tango import Database, DbDevInfo
import air_sensor

dev_info = DbDevInfo()
dev_info.server = "AirSensor/test"
dev_info._class = "AirSensor"
dev_info.name = "test/air_sensor/1"
db = Database()
db.add_device(dev_info)
