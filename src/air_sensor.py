from time import time

from tango import AttrQuality, AttrWriteType, DispLevel, AttReqType
from tango.server import Device, attribute, command
from tango.server import class_property, device_property

class AirSensor(Device):
    # temperature = attribute(label="Temperature",
    #                         dtype=float,
    #                         display_level=DispLevel.EXPERT,
    #                         unit="degC", format="8.4f",
    #                         min_value=-30.0, max_value=100.0,
    #                         min_alarm=-1.0, max_alarm=70.0,
    #                         min_warning=0.0, max_warning=50.0,
    #                         fget="get_temperature",
    #                         doc="The room temperature")

    host = device_property(dtype=str)
    port = class_property(dtype=int, default_value=9788)

    @attribute
    def voltage(self):
        self.info_stream("get voltage(%s, %d)" % (self.host, self.port))
        return 10.0

    @attribute
    def temperature(self):
        return 25.0, time(), AttrQuality.ATTR_WARNING


if __name__ == "__main__":
    AirSensor.run_server()