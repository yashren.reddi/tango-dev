from tango import DeviceProxy

tango_test = DeviceProxy("test/air_sensor/1")

print(tango_test.ping())

print(tango_test.state())

print(tango_test.temperature)